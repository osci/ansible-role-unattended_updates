Ansible module that setup platform appropriate update scripts

To use it, just include the role.

By default security as well as point release updates are installed
automatically. On Linux systems, if you wish you can set
`unattended_security_updates_only` to True to limit automatic
installations to security updates. This feature unfortunately does
not work on Centos (EPEL would, not the base repos), see:
  https://www.centos.org/forums/viewtopic.php?t=59369#p251143
  https://centosfaq.org/centos/yum-security-not-detecting-security-updates/

